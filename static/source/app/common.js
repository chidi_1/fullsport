$(function(){
   if($('.slider').length){
        $('.slider').owlCarousel({
            margin:0,
            loop: true,
            autoWidth:false,
            nav: false,
            dots: true,
            autoplay: true,
            autoplayTimeout: 2000,
            animateOut: 'fadeOut',
            responsiveClass:true,
            items:1
        });
   }
   if($('.carousel').length){
        $('.carousel').owlCarousel({
            margin:0,
            loop: true,
            autoWidth:false,
            nav: true,
            dots: false,
            autoplay: false,
            navText: [,],
            responsiveClass: true,
            responsive: {
                0:{
                    items:2
                },
                480:{
                    items:3
                },
                769: {
                    items: 4
                },
                979: {
                    items: 5
                }
            }
        });
   }

   if($('.about-slider').length){
        $('.about-slider').owlCarousel({
            margin:0,
            loop: true,
            autoWidth:false,
            nav: false,
            dots: false,
            autoplay: true,
            animateOut: 'fadeOut',
            items:1
        });
   }

   $(".fancybox").fancybox();

   $(document).on('click', '.js--show-basket-popup', function(){
       $('.header__basket__popup').stop();
       if($(this).parents('.header__basket').hasClass('basket-empty')){
           $('.popup-empty').fadeIn(150)
       }else{
            $('.popup-full').fadeIn(150)
       }
       setTimeout(function(){
            $('.header__basket__popup').fadeOut(300)
       }, 5000);
       return false;
   });
   $(document).click(function(){$('.header__basket__popup').fadeOut(300)})


   var screenshotPreview = function(){
	/* CONFIG */
		$('body').mousemove(function(g) {
            var x = g.screenX;
			var y = g.screenY;
			var scrW = $("#screenshot").width();
			var scrH = $("#screenshot").height();
			if(x <= 600){
					var set = 70;
					var shet = (scrH/2);
					xOffset = shet;
					yOffset = set;

		} else  {
			var set = scrW+70;
			var shet = (scrH/2);
			xOffset = shet;
			yOffset = -set;

			}
			if (y >=700) {
				var shet = scrH+70;
				xOffset = shet;
				} else if (y <=250) {
					var shet = (scrH/4)-50;
					xOffset = shet;

					}

        });

		// these 2 variable determine popup's distance from the cursor
		// you might want to adjust to get the right result

	/* END CONFIG */

        $("a.screenshot").click(function(e){
            window.location = $(this).attr("href")

        })
        $("a.screenshot").hover(function(e){

            this.t = this.title;
            this.title = "";
            var c = (this.t != "") ? "<br/>" + this.t : "";
            $("body").append("<p id='screenshot'><img src='"+ this.rel +"' alt='url preview' />"+ c +"</p>");
            $("#screenshot")
                .css("top",(e.pageY - xOffset) + "px")
                .css("left",(e.pageX + yOffset) + "px")
                .fadeIn("slow");
        },
        function(){
            this.title = this.t;
            $("#screenshot").remove();
        });
        $("a.screenshot").mousemove(function(e){
            $("#screenshot")
                .css("top",(e.pageY - xOffset) + "px")
                .css("left",(e.pageX + yOffset) + "px");
        });
    };
    // starting the script on page load
    $(document).ready(function(){
        screenshotPreview();
        $('.prev_img_index').mouseover(function(e) {
                $(this).animate({borderRadius:'5%'},'slow','linear');
        });
        $('.prev_img_index').mouseout(function(e) {
                $(this).animate({borderRadius:'50%'},'slow','linear');
        });
    });

      $(document).on('click', '.js--product-add', function(){
        var form = $(this).parents('.product-form');
        var method = form.attr('method');
        var action = form.attr('action');
        var data = form.serialize();
        var product_id = form.attr('data_id');
        $.ajax({
            url: action,
            method: method,
            data: data,
            success: function (data) {
                if( $('.header__basket').hasClass('basket-empty')) {
                    $('.header__basket').toggleClass('basket-empty basket-full');
                }
                $('.popup-full').empty().append(data);
                var counter = parseInt($('.popup-full__total-number').text());
                $('.header__basket-text i').text(counter);
                $('.js--show-basket-popup').trigger('click');
            }
        });
        return false;
    })

    // конструктор
    $(document).on('click', '.dial-next', function(){
        var dial_length = $('.constructor-content__dial li').length;
        var active_dial = $('.constructor-content__dial li.active').index() + 1;
        $('.constructor-content__dial li').removeClass('active');
        if(dial_length == active_dial){
            $('.constructor-content__dial li').eq(0).addClass('active')
        }else{
            $('.constructor-content__dial li').eq(active_dial).addClass('active')
        }
        change_img();
    });
    $(document).on('click', '.dial-prev', function(){
        var dial_length = $('.constructor-content__dial li').length;
        var active_dial = $('.constructor-content__dial li.active').index();
        $('.constructor-content__dial li').removeClass('active');
        if(dial_length == 0){
            $('.constructor-content__dial li').eq(dial_length).addClass('active')
        }else{
            $('.constructor-content__dial li').eq(active_dial - 1).addClass('active')
        }
        change_img();
    });
    $(document).on('click', '.bracelet-next', function(){
        var dial_length = $('.constructor-content__bracelet li').length;
        var active_dial = $('.constructor-content__bracelet li.active').index() + 1;
        $('.constructor-content__bracelet li').removeClass('active');
        if(dial_length == active_dial){
            $('.constructor-content__bracelet li').eq(0).addClass('active')
        }else{
            $('.constructor-content__bracelet li').eq(active_dial).addClass('active')
        }
        change_img();
    });
    $(document).on('click', '.bracelet-prev', function(){
        var dial_length = $('.constructor-content__bracelet li').length;
        var active_dial = $('.constructor-content__bracelet li.active').index();
        $('.constructor-content__bracelet li').removeClass('active');
        if(dial_length == 0){
            $('.constructor-content__bracelet li').eq(dial_length).addClass('active')
        }else{
            $('.constructor-content__bracelet li').eq(active_dial - 1).addClass('active')
        }
        change_img();
    });
    $(document).on('click', '.constructor-content__bracelet li', function(){
        $('.constructor-content__bracelet li').removeClass('active');
        $(this).addClass('active');
        change_img();
    });
    $(document).on('click', '.constructor-content__dial li', function(){
        $('.constructor-content__dial li').removeClass('active');
        $(this).addClass('active');
        change_img();
    });
    var  change_img = function(){
        var dial_id = $('.constructor-content__dial li.active').attr('data-id');
        var dial = $('.constructor-content__dial li.active').attr('data-img');
        var dial_name = $('.constructor-content__dial li.active').attr('data-name');
        var bracelet_id = $('.constructor-content__bracelet li.active').attr('data-id');
        var bracelet = $('.constructor-content__bracelet li.active').attr('data-img');
        var bracelet_name = $('.constructor-content__bracelet li.active').attr('data-name');

        $('.constructor-img-dial').attr('src',dial);
        $('.constructor-img-braclet').attr('src',bracelet);
        $('.constructor-info-dial .value').text(dial_name);
        $('.constructor-info-braclet .value').text(bracelet_name);
        $('.conscructor-form .input-dial').prop('value',dial_id);
        $('.conscructor-form .input-bracelet').prop('value',bracelet_id);
        $('.product-form .input-dial').prop('value',dial_id);
        $('.product-form .input-bracelet').prop('value',bracelet_id);
        $('.constructor-info-size ul li').each(function(){
            $(this).removeClass('active')
        });

        var form = $('.conscructor-form');
        var method = form.attr('method');
        var action = form.attr('action');
        var data = form.serialize();
        $.ajax({
            url: action,
            method: method,
            data: data,
            success: function (data) {
                var parse_data = jQuery.parseJSON(data);
                if(parse_data.xs == "true"){$('.xs input').removeAttr('disabled')}else{$('.xs input').attr('disabled',true)}
                if(parse_data.s == "true"){$('.s input').removeAttr('disabled')}else{$('.s input').attr('disabled',true)}
                if(parse_data.m == "true"){$('.m input').removeAttr('disabled')}else{$('.m input').attr('disabled',true)}
                if(parse_data.l == "true"){$('.l input').removeAttr('disabled')}else{$('.l input').attr('disabled',true)}

                var has_active = false;
                $('.constructor-info-size div label').each(function(){
                    if($(this).find('input').attr('disabled') == undefined && has_active == false){
                        $(this).find('input').attr('checked','checked');
                        has_active = true;
                    }
                });
                $('.constructor-pay a i').text(parse_data.price);
            }
        });
        return false;
    }

    // ввод только цифр в поле
    var input_number = function(){
        var allow_meta_keys=[86, 67, 65];
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9  || event.keyCode == 27 ||
                // Разрешаем: Ctrl+A
                ($.inArray(event.keyCode,allow_meta_keys) > -1 && (event.ctrlKey === true ||  event.metaKey === true)) ||
                // Разрешаем: home, end, влево, вправо
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                // Ничего не делаем
                return;
            }
            else {
                // Обеждаемся, что это цифра, и останавливаем событие keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
    };
    // ввод только цифр в корзине
    $(document).on('keydown', '.input_number', function(e){input_number();})

    // изменение количества товаров в корзине
    $(document).on('click', '.js--amount-refresh', function() {
        var current_position = $(this).parents('tr');
        var item = current_position.find('.basket-item').prop('value');
        var amount = current_position.find('.basket__line-number input').prop('value');
        var url = $('.basket').attr('data-url');
        var method = $('.basket').attr('data-method');
        if (amount != '') {
                $.ajax({
                    url: url,
                    method: method,
                    data: {item: item, amount: amount},
                    success: function (data) {
                        var parse_data = jQuery.parseJSON(data);
                        current_position.find('.basket__line-total span').text(parse_data.position + " руб.")
                        $('.table-sub-total .basket__line-total span').text(parse_data.subtotal + " руб.")
                        $('.basket__total .basket__total-total span').text(parse_data.total + " руб.")
                        $('.basket__discount .basket__line-total span').text(parse_data.discount + " руб.")

                        $('.popup-full').empty().append(parse_data.header_html);
                        $('.header__basket-text i').text(parse_data.header_amount);
                    }
                });
        }
        return false;
    });

    // удалить позицию в корзине
    $(document).on('click', '.js--amount-delete', function() {
        var current_position = $(this).parents('tr');
        var form = $(this).parents(form)
        var url = $('.basket').attr('data-url');
        var method = $('.basket').attr('data-method');
        $.ajax({
            url: url,
            method: method,
            data: form.serialize(),
            success: function (data) {
                var parse_data = jQuery.parseJSON(data);
                current_position.remove();
                $('.table-sub-total .basket__line-total span').text(parse_data.subtotal + " руб.")
                $('.basket__total .basket__total-total span').text(parse_data.total + " руб.")
                $('.basket__discount .basket__line-total span').text(parse_data.discount + " руб.")

                $('.popup-full').empty().append(parse_data.header_html);
                $('.header__basket-text i').text(parse_data.header_amount);

                if($('.basket tr').length == 4){
                    $('.basket-wrap').addClass('hidden-block').next('.basket-empty-text').removeClass('hidden-block')
                }
            }
        });

        return false;
    });

    // карта
    if($('#map').length){
        var myMap;
        function init() {
            myMap = new ymaps.Map('map', {
                center: [55.681469,37.652416],
                zoom: 16,
                controls: ['zoomControl']
            });
            myMap.controls.add('zoomControl', { left: 5, top: 5 });

            myMap.behaviors.disable('scrollZoom');
            myMap.geoObjects.add(new ymaps.Placemark([55.681469,37.652416], {}, {
                preset: 'islands#dotIcon',
                iconColor: '#a5260a'
            }));
        }
        ymaps.ready(init);
    }

    // выпадайка меню
    $(document).on('click', '.js--show-nav', function(){
        $('.main-nav').slideToggle(200)
    });

    // отправка простых форм
    // валидация почты
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

     $(document).on('click', '.js-form-submit', function () {
        var form =  $(this).parents('.mail-form');
        var errors = false;

        $(form).find('.required').each(function(){
            var val=$(this).prop('value');
            if(val==''){
                $(this).addClass('error');
                errors=true;
            }
            else{
                if($(this).hasClass('input-mail')){
                    if(validateEmail(val) == false){
                        $(this).addClass('error');
                        errors=true;
                    }
                }
            }
        });

        if(errors == false){
            var button_value = $(form).find('.js-form-submit').text();
            $(form).find('.js-form-submit').text('Подождите...');

            var method= form.prop('method');
            var action= form.prop('action');
            $.ajax({
                type: method,
                url: action,
                data: form.serialize(),
                success: function(data) {
                    var parse_data = jQuery.parseJSON(data);
                    if(parse_data.error == 'true'){
                        $('.error-message').removeClass("hidden-block");
                        $(form).find('.js-form-submit').text('Произошла ошибка');
                        setTimeout(function(){
                            $('.error-message').addClass("hidden-block");
                            $(form).find('.js-form-submit').text(button_value);
                        },3000)
                    }else{
                        window.location = data.location
                    }
                },
                error: function(data) {
                    $(form).find('.js-form-submit').text('Произошла ошибка');
                    setTimeout(function() {
                        $(form).find('.js-form-submit').text(button_value);
                    }, 2000);
                }
            });
        }
        return false;
    });

    $(document).on('focus', '.form-input', function(){
        $(this).removeClass('error');
    });

    // ввод только цифр в поле телефона
	$(document).on('keydown', '.input-phone', function(e){input_number();});
    // ввод только цифр в поле
    var input_number = function(){
        var allow_meta_keys=[86, 67, 65];
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9  || event.keyCode == 27 ||
            // Разрешаем: Ctrl+A
            ($.inArray(event.keyCode,allow_meta_keys) > -1 && (event.ctrlKey === true ||  event.metaKey === true)) ||
            // Разрешаем: home, end, влево, вправо
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // Ничего не делаем
            return;
        }
        else {
            // Обеждаемся, что это цифра, и останавливаем событие keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    };

    $(document).on('click', '.form-delivery__type label', function(){
        $('.form-delivery__type .label-hidden input').each(function(){
            $(this).removeClass('required')
        });
        $(this).find('.label-hidden input').each(function(){
            $(this).addClass('required')
            $('.input-housing').removeClass('required')
            $('.input-flat').removeClass('required')
        })
    })

});